import { asLiteral } from '@angular/compiler/src/render3/view/util';
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'pathfinding';

  xCount = 10;
  yCount = 10;

  openedList = []
  closedList = []
  safeHome: any = [{ x: 2, y: 5 }, { x: 8, y: 4 }];
  finalPath: any = []

  droneMissionPath: any = []
    = []
  // = [ { "x": 2, "y": 5 }, { "x": 3, "y": 5 }, { "x": 3, "y": 6 }, { "x": 3, "y": 7 }, { "x": 4, "y": 7 }, { "x": 5, "y": 7 }, { "x": 5, "y": 6 }, { "x": 5, "y": 5 }, { "x": 6, "y": 5 }, { "x": 6, "y": 4 }, { "x": 6, "y": 3 }, { "x": 6, "y": 2 }, { "x": 6, "y": 1 }, { "x": 6, "y": 0 }, { "x": 5, "y": 0 }, { "x": 4, "y": 0 }, { "x": 4, "y": 1 } ]

  noWalkableList = []
  // = [{ "x": 1, "y": 1 }, { "x": 1, "y": 2 }, { "x": 1, "y": 3 }, { "x": 1, "y": 4 }, { "x": 1, "y": 5 }, { "x": 2, "y": 2 }, { "x": 2, "y": 3 }, { "x": 3, "y": 3 }, { "x": 3, "y": 4 }, { "x": 4, "y": 3 }, { "x": 4, "y": 4 }, { "x": 4, "y": 5 }, { "x": 4, "y": 6 }, { "x": 5, "y": 4 }, { "x": 5, "y": 1 }]
  // = [{ x: 3, y: 6 }, { x: 4, y: 2 }]

  x = []
  y = []

  finished = false;

  // start = { x: 3, y: 1 };
  // finish = { x: 9, y: 5 };

  start = { x: 0, y: 0 };
  // finish = { x: 4, y: 8 };

  finish = { x: 1, y: 0 };

  current;


  iterationCounter = 0;
  noPath = false;

  debugMode = true;
  mode;





  async ngOnInit() {
    this.populateWidthAndHeight()
  }

  modeChoose(mode) {
    this.mode = mode;
  }

  reset() {
    this.closedList = [];
    this.openedList = [];
    this.finalPath = []
  }

  selectNode(x, y) {
    let noWalkableIndex
    switch (this.mode) {
      case "end":
        if (this.isCordExists(x, y)) this.finish = { x, y };
        break
      case "obstacle":
        noWalkableIndex = this.isNoWalkable(x, y);
        if (noWalkableIndex !== -1) {
          this.noWalkableList.splice(noWalkableIndex, 1)
          break
        }
        if (this.isCordExists(x, y)) {
          this.noWalkableList.push({ x, y })
        }
        break;

      case "mission":
        noWalkableIndex = this.isNoWalkable(x, y);
        if (noWalkableIndex !== -1) {
          this.droneMissionPath.splice(noWalkableIndex, 1)
          break
        }
        if (this.isCordExists(x, y)) {
          this.droneMissionPath.push({ x, y })
        }
        break;
      case "start":
        if (this.isCordExists(x, y)) this.start = { x, y };
        break
    }
  }

  async startPathfinding() {
    this.x = []
    this.y = []
    this.populateWidthAndHeight()

    this.current = this.start;
    while (!this.finished && !this.noPath) {
      if (this.iterationCounter >= (this.xCount * this.yCount) - this.noWalkableList.length - 1) {
        console.log("NE POSTOJI RUTA")
        this.noPath = true;
      }
      if (this.finished || this.noPath) continue;
      await this.sleep()
      this.markSideNodes();
      this.getNext();
      this.iterationCounter++
    }

    if (this.finished) {
      this.getFinalPath();
    }
  }

  nextIteration() {
    this.markSideNodes();
    this.getNext();
  }

  markSideNodes() {
    let sideNodes = this.getSideNodesCordinates(this.current.x, this.current.y)
    for (let i = 0; i < sideNodes.length; i++) {
      const nodeCord = sideNodes[i];
      if (!this.isValid(nodeCord.x, nodeCord.y)) continue;
      let next = {
        x: nodeCord.x,
        y: nodeCord.y,
        g: this.getG(),
        h: this.calculateH(nodeCord.x, nodeCord.y),
        f: this.calculateH(nodeCord.x, nodeCord.y) + this.getG()
      }
      this.openedList.push(next)
      if (this.isEnd(nodeCord.x, nodeCord.y)) {
        this.finished = true;
        this.closedList.push(next)
        return
      }
    }
  }

  getNext() {
    let indexOfSmallest;
    for (let i = 0; i < this.openedList.length; i++) {
      const node = this.openedList[i];
      if (indexOfSmallest === undefined || this.openedList[indexOfSmallest].f > node.f) indexOfSmallest = i
    }

    this.current = this.openedList[indexOfSmallest];
    if (this.current !== null && this.current !== undefined) {
      this.closedList.push(this.current)
    }
    this.openedList.splice(indexOfSmallest, 1)
  }

  isValid(x, y) {
    if (this.isInOpenedList(x, y) === -1 && this.isInClosedList(x, y) === -1 && this.isCordExists(x, y) && !this.isStart(x, y) && this.isNoWalkable(x, y) === -1) {
      return true;
    }
    return false
  }
  getG() {
    if (this.closedList.length === 0) return 1;
    else return this.closedList[this.closedList.length - 1].g + 1
  }

  isCordExists(x, y) {
    if (this.x[x] === undefined || this.y[y] === undefined) return false;
    return true;
  }

  isInOpenedList = (x, y) => {
    for (let i = 0; i < this.openedList.length; i++) {
      const node = this.openedList[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  isInClosedList = (x, y) => {
    for (let i = 0; i < this.closedList.length; i++) {
      const node = this.closedList[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  isNoWalkable(x, y) {
    for (let i = 0; i < this.noWalkableList.length; i++) {
      const node = this.noWalkableList[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  populateWidthAndHeight = () => {
    for (let i = 0; i < this.xCount; i++) {
      this.x.push({})
    }

    for (let i = 0; i < this.yCount; i++) {
      this.y.push({})
    }
  }

  isCurrent(x, y) {
    if (!this.current) return false;
    return this.current.x === x && this.current.y === y
  }

  isStart(x, y) {
    return this.start.x === x && this.start.y === y
  }

  isEnd(x, y) {
    return this.finish.x === x && this.finish.y === y
  }

  findG(x, y) {
    let openedListIndex = this.isInOpenedList(x, y);
    let closedListIndex = this.isInClosedList(x, y);

    if (openedListIndex !== -1) return this.openedList[openedListIndex].g
    if (closedListIndex !== -1) return this.closedList[closedListIndex].g
    return -1

  }

  findH(x, y) {
    let openedListIndex = this.isInOpenedList(x, y);
    let closedListIndex = this.isInClosedList(x, y);

    if (openedListIndex !== -1) return this.openedList[openedListIndex].h
    if (closedListIndex !== -1) return this.closedList[closedListIndex].h
    return -1

  }

  calculateH(x, y) {
    let h = Math.sqrt(Math.pow((this.finish.x - x), 2) + Math.pow((this.finish.y - y), 2));
    return Math.round(Math.abs(h) * 100) / 100
  }

  calculateF(x, y) {
    let openedListIndex = this.isInOpenedList(x, y);
    let closedListIndex = this.isInClosedList(x, y);

    if (openedListIndex !== -1) return Math.round(this.openedList[openedListIndex].f * 10000) / 10000
    if (closedListIndex !== -1) return Math.round(this.closedList[closedListIndex].f * 10000) / 10000
    return -1
  }

  isInFinal = (x, y) => {
    for (let i = 0; i < this.finalPath.length; i++) {
      const node = this.finalPath[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  isInDronMissionPath = (x, y) => {
    for (let i = 0; i < this.droneMissionPath.length; i++) {
      const node = this.droneMissionPath[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  isInSafeHome = (x, y) => {
    for (let i = 0; i < this.safeHome.length; i++) {
      const node = this.safeHome[i];
      if (node.x === x && node.y === y) return i;
    }

    return -1
  }

  async getFinalPath() {
    let foundedStart = false

    let currentNode = this.finish;
    let finishNodeIndex = this.isInClosedList(this.finish.x, this.finish.y)
    let currentStep = this.closedList[finishNodeIndex].g;
    let currentF = this.closedList[finishNodeIndex].f;
    let currentFounded = false;
    while (!foundedStart) {
      currentFounded = false;
      let sides = this.getSideNodesCordinates(currentNode.x, currentNode.y)
      for (let j = 0; j < sides.length; j++) {
        const node = sides[j];
        if (this.isStart(node.x, node.y)) {
          foundedStart = true;
          break;
        }
        let closedIndex = this.isInClosedList(node.x, node.y)

        if (closedIndex === -1 || !this.isCordExists(node.x, node.y)) continue;

        if (this.closedList[closedIndex].g <= currentStep && this.closedList[closedIndex].f <= currentF) {
          currentStep--;
          currentF = this.closedList[closedIndex].f;
          currentNode = this.closedList[closedIndex];
          currentFounded = true;
        }
      }
      if (currentFounded) {
        this.finalPath.push(currentNode);
        await this.sleep()
        continue;
      }

      if (!currentFounded && !foundedStart) {
        let smallestStepIndex;

        for (let j = 0; j < sides.length; j++) {
          const node = sides[j];
          let closedIndex = this.isInClosedList(node.x, node.y)
          if (closedIndex === -1 || !this.isCordExists(node.x, node.y)) continue;

          if (!smallestStepIndex) {
            smallestStepIndex = closedIndex;
            continue;
          }

          if (this.closedList[closedIndex].g <= this.closedList[smallestStepIndex].g) {
            smallestStepIndex = closedIndex;
            continue
          }
        }

        this.finalPath.push(this.closedList[smallestStepIndex]);

        currentStep--;
        currentF = this.closedList[smallestStepIndex].f;
        currentNode = this.closedList[smallestStepIndex];
        await this.sleep()

      }


    }
  }

  async sleep(millis?) {
    if (!millis) millis = 50
    return new Promise(resolve => setTimeout(resolve, millis));
  }

  getSideNodesCordinates(x, y) {

    let sides = [];
    sides.push({ x: x, y: y - 1 })
    sides.push({ x: x + 1, y: y - 1 })
    sides.push({ x: x + 1, y: y })
    sides.push({ x: x + 1, y: y + 1 })
    sides.push({ x: x, y: y + 1 })
    sides.push({ x: x - 1, y: y + 1 })
    sides.push({ x: x - 1, y: y })
    sides.push({ x: x - 1, y: y - 1 })

    return sides;
  }

  async determineDirectionOfFinalPath() {
    let currentDirection;
    for (let i = 0; i < this.finalPath.length; i++) {
      const node = this.finalPath[i];
      let nextNode = this.finalPath[i + 1]
      if (nextNode === undefined) {
        nextNode = this.start;
      }
      let side = this.compareCordinate(node, nextNode);
      node.side = Sides[side]
    }
  }

  async determineDirectionOfDronePath() {
    let currentDirection;
    for (let i = 0; i < this.droneMissionPath.length; i++) {
      const node = this.droneMissionPath[i];
      let nextNode = this.droneMissionPath[i + 1]
      if (nextNode === undefined) {
        nextNode = this.start;
      }
      let side = this.compareCordinate(node, nextNode, "mission");
      node.side = Sides[side]
    }
  }

  compareCordinate(current, next, mode?) {
    let sidesOfCurrent = this.getSideNodesCordinates(current.x, current.y);
    for (let i = 0; i < sidesOfCurrent.length; i++) {
      const node = sidesOfCurrent[i];
      if (this.isStart(node.x, node.y) && mode !== "mission") return i;
      if (node.x === next.x && node.y === next.y) return i;
    }
  }

  getDirectionFinalPath(x, y) {
    let index = this.isInFinal(x, y);

    if (this.finalPath[index] === undefined) return null;
    return this.finalPath[index]["side"]
  }

  getDirectionDronePath(x, y) {
    let index = this.isInDronMissionPath(x, y);
    if (this.droneMissionPath[index] === undefined) return null;
    return this.droneMissionPath[index]["side"]
  }

  //=======================================================


  getMinimalSafeLandingPosition() {
    this.determineDirectionOfDronePath()

    this.safeHome = [...this.safeHome, this.start, this.finish];
    let noSafeHomeNodeHolder = undefined;

    for (let i = 0; i < this.droneMissionPath.length; i++) {
      const node = this.droneMissionPath[i];

      console.log(noSafeHomeNodeHolder)
      if (noSafeHomeNodeHolder !== undefined) {
        console.log(1)
        if (node.side === noSafeHomeNodeHolder.side) continue
        else {
          console.log("SAFE POSITION", node)
          this.safeHome.push(node)
          noSafeHomeNodeHolder = undefined;
        }
      }

      if (this.isSafePositionConnected(node.x, node.y)) {
        console.log("Ima", node)
      } else {
        console.log("Nema", node)
        noSafeHomeNodeHolder = node;

      }

    }
  }

  isSafePositionConnected(x, y) {
    this.safeHome = [...this.safeHome, this.start, this.finish];

    let currentX = x, currentY = y;
    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentY--
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX++;
      currentY--
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX++;
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX++;
      currentY++
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentY++
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX--;
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX--;
      currentY--
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX;
      currentY--
    }
    currentX = x, currentY = y;

    while (this.isCordExists(currentX, currentY) && this.isNoWalkable(currentX, currentY) === -1) {
      if (this.isInSafeHome(currentX, currentY) !== -1) {
        return true;
      }
      currentX;
      currentY--
    }
    currentX = x, currentY = y;

  }
}

enum Sides {
  N = 0,
  NE = 1,
  E = 2,
  SE = 3,
  S = 4,
  SW = 5,
  W = 6,
  NW = 7,
}
